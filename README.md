# Photo processor

The aim of this proof of concept is to orchestrate the generation of thumbnails for a specific set of photos.

# Installation

Prerequisites:
* Docker
* python 3.7 + main modules:
    * requests
    * flask
    * sqlalchemy
    * pillow
    * pIka
* Ability to run `make`
* virtualenv

Generate virtualenv and install requirements

    pipenv update

Once all installed activate current virtualenv

    pipenv shell


## Start

    make start

once app is up and runing you have to run DB fixes. Create or reset the DB schema after booting the app:

    make db-schema

Postgres PSQL can be accessed via:

    make psql

RabbitMQ management console can be accessed at:

    http://localhost:15672/


# Architecture


    +---------------------------+                                       +---------------------------------------+
    |                           |                                       |                                       |
    |                           |                                       |        Processor thread               |
    |                           |                                       |                                       |
    |    Web server             |                                       |                                       |
    |    API                    |                                       |                                       |
    |                           |                                       +---------------------------------------+
    |                           |
    |                           |                                              ^                   +
    |                           |                                              |                   |
    +-------------+-------------+                                              |                   |
                  |                                                            |                   v
                  |                                                            |
                  |                                                            |              +-------------+
    +-------------v-------------+                                              |              |             |
    |                           |                        +---------------------++             |             |
    |                           |                        |                      |             | local       |
    | Processing image request  |                        |                      |             | storage     |
    |                           | +------------------->  |     AMQP             |             |             |
    +---------------------------+                        |     (Rabbit MQ)      |             +-------------+
                                                         |                      |
                                                         |                      |
                                                         +----------------------+




# Usage

Main end point for this simple API is

    http://localhost:3000

## Pending photos list

List of pending images to be processed


> GET /photos/pending/

Expected response

    [
      {
        "UUID": "22d27fa6-c347-4c3c-a93c-7b7c858bee4d",
        "url": "https://s3.amazonaws.com/waldo-thumbs-dev/large/71840919-e422-552d-8c8d-9b2b360ce98c.jpg"
      },
      {
        "UUID": "a41553f6-0892-441b-a849-f75193c759f3",
        "url": "https://s3.amazonaws.com/waldo-thumbs-dev/large/72800f95-c406-5475-85ac-b8943877b15f.jpg"
      },

      ...

    ]


## Pending photo details

Details of pending image

> GET /photos/pending/< uuid >/

Expected result


    {
        "UUID": "22d27fa6-c347-4c3c-a93c-7b7c858bee4d",
        "url": "https://s3.amazonaws.com/waldo-thumbs-dev/large/71840919-e422-552d-8c8d-9b2b360ce98c.jpg"
    }

## Completed

List of images that are already processed

> GET /photos/completed/

Expected response


    [
      {
        "UUID": "22d27fa6-c347-4c3c-a93c-7b7c858bee4d",
        "url": "https://s3.amazonaws.com/waldo-thumbs-dev/large/71840919-e422-552d-8c8d-9b2b360ce98c.jpg"
      },
      {
        "UUID": "a41553f6-0892-441b-a849-f75193c759f3",
        "url": "https://s3.amazonaws.com/waldo-thumbs-dev/large/72800f95-c406-5475-85ac-b8943877b15f.jpg"
      },

      ...

    ]

## Download and process

Processing image means:

* fetch and download image by given UUID
* store on locally mounted storage `/tmp/waldo-app-thumbs`
* generate image thumbnail and store under the same mounted resource (with suffix `-small`)


To make system to download given image UUID you have to send POST message as below to endpoint

> POST /photos/process/

payload

    {
        "UUID": [
            "1f6b723b-b31c-43c7-955e-8c9f603c1698",
            "7e9aee05-f2d8-4a99-818c-cff1512636db"
        ]
    }


example curl command

    curl -H "Content-Type: application/json" -X POST -d '{"UUID": ["1f6b723b-b31c-43c7-955e-8c9f603c1698", "7e9aee05-f2d8-4a99-818c-cff1512636db"]}' "http://127.0.0.1:3000/photos/process/"

## DB storage

Once download and processing is finished new record in table `photo_thumbnails` is created.

## Error

When processing or downloading fails with an error `photos` table record is going to be updated with status `failed`

# Testing

To run unit test just execute below

    make test

Notice testing is only working locally not as container app. So to start testing as prerequisites:

* pipenv install
* run tests
* green results :)

# Additional things to consider

## Serializers

If this would be production app strongly recommended it is to use rest-api serialzers which can do better mapping between DB models and JSON payloads

## DB structure

This project assumes because it's just a proof of concept, that no records exist in DB so if you see any duplication errors - please ignore.
