from environs import Env


def get_default_env():
    env = Env()
    env.STORAGE_DIR = env.str("STORAGE_DIR", "/waldo-app-thumbs")
    env.AMQP_USERNAME = env.str('AMQP_USERNAME', 'rabbitmq')
    env.AMQP_PASSWORD = env.str('AMQP_PASSWORD', '1234')
    env.AMQP_HOST = env.str('AMQP_HOST', 'localhost')
    env.AMQP_URI = env.str('AMQP_URI', 'amqp://{0}:{1}@{2}:5672/%2f'.format(env.AMQP_USERNAME, env.AMQP_PASSWORD, env.AMQP_HOST))
    env.AMQP_HOST = env.str('AMQP_HOST', 'localhost')
    env.PG_HOST = env.str('PG_HOST', 'postgres')
    env.PG_USERNAME = env.str('PG_USERNAME', 'waldo')
    env.PG_PASSWORD = env.str('PG_PASSWORD', '1234')
    env.PG_DATABASE = env.str('PG_DATABASE', 'waldo')
    env.PG_CONNECTION_URI = env.str('PG_CONNECTION_URI', 'postgresql://{}:{}@{}/{}'.format(
        env.PG_USERNAME, env.PG_PASSWORD, env.PG_HOST, env.PG_DATABASE)
    )
    env.PHOTO_QUEUE_NAME = env.str('PHOTO_QUEUE_NAME', 'photo-proccessor')
    return env

def init_config():
    env = get_default_env()
    try:
        env.read_env('.env')
    except OSError:
        pass
    return env
