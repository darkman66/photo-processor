import datetime
import uuid
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Enum, DateTime
from sqlalchemy.dialects.postgresql import UUID


Base = declarative_base()

STATUS_CHOICES = (
    'pending',
    'completed',
    'processing',
    'failed'
)


def get_new_uuid():
    """ Return a new UUID in standard format. """
    return str(uuid.uuid4())


class Photos(Base):
    __tablename__ = 'photos'

    uuid = Column(UUID(as_uuid=True), primary_key=True, unique=True, nullable=False)
    url = Column(String)
    status = Column("status", Enum(*STATUS_CHOICES, name="status", create_type=False))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)


class PhotoThumbnails(Base):
    __tablename__ = 'photo_thumbnails'

    uuid = Column(UUID(as_uuid=True), primary_key=True, unique=True, nullable=False)
    photo_uuid = Column(UUID(as_uuid=True), ForeignKey('photos.uuid'), nullable=False)
    url = Column(String)
    width = Column(Integer, default=0)
    height = Column(Integer, default=0)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
