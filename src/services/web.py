import os
import pika
import threading
import signal
import requests
import logging
from PIL import Image
from werkzeug.serving import run_simple
from config import init_config
from flask import Flask, jsonify
from flask import abort
from models import Photos, PhotoThumbnails
from flask import jsonify
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from models import Photos, PhotoThumbnails, get_new_uuid
from sqlalchemy.orm.exc import NoResultFound
from flask import request
from pika.exceptions import StreamLostError, ConnectionWrongStateError


app = Flask(__name__)
cfg = init_config()


class MyThread(threading.Thread):
    """Simple threading to consume incoming calls"""

    def __init__(self):
        super(MyThread, self).__init__(target=self.start_consuming)
        app.logger.info("Starting consumer")
        self.connection = pika.BlockingConnection(connection_parameters)
        self.ch = self.connection.channel()
        self.ch.basic_consume(queue=cfg.PHOTO_QUEUE_NAME, on_message_callback=process_photo, auto_ack=True)
        self._please_stop = threading.Event()

    @property
    def stopped(self):
        return self._please_stop.is_set()

    def start_consuming(self):
        while self.ch._consumer_infos and not self.stopped:
            try:
                self.ch.connection.process_data_events(time_limit=1)
            except StreamLostError:
                break

    def stop(self):
        self._please_stop.set()


def convert(uuid, width, height):
    src_path = os.path.join(cfg.STORAGE_DIR, uuid)
    dst_path = os.path.join(cfg.STORAGE_DIR, uuid)
    dst_path += '-small.jpg'

    im = Image.open(src_path)
    out = im.resize((width, height), Image.ANTIALIAS)
    out.save(dst_path)
    return dst_path


def process_photo(ch, method, properties, uuid):
    uuid = str(uuid, "utf-8")
    app.logger.info("-> Processing UUID %s" % uuid)
    photo = session.query(Photos).filter_by(uuid=uuid).one()

    try:
        photo.status = 'pending'
        session.commit()

        r = requests.get(photo.url, allow_redirects=True)
        file_path = os.path.join(cfg.STORAGE_DIR, '{}'.format(photo.uuid))
        open(file_path, 'wb').write(r.content)
        app.logger.info('File size: %s', len(r.content))
        width = 320
        height = 320
        dst_path = convert(uuid, width, height)

        photo.status = 'completed'
        session.commit()

        photo_thumbnails = PhotoThumbnails(
            uuid=get_new_uuid(),
            photo_uuid=photo.uuid,
            url=dst_path,
            width=width,
            height=height
        )

        session.add(photo_thumbnails)
        session.flush()

        app.logger.info('Processing photo UUID: %s is done', uuid)
    except Exception as e:
        app.logger.error(e)
        photo.status = 'failed'
        session.commit()

def close_active_queue(signum, frame):
    print("Stopping main queue processor thread")
    consumer_task.stop()
    print("processor stopped...")

    try:
        task_processor_connection.close()
    except StreamLostError:
        pass
    except ConnectionWrongStateError:
        pass
    print("Closing active queues DONE")

    # hacky and nasty but need to force debug server to shut down
    raise RuntimeError("Server going down")


@app.route("/")
def index():
    return jsonify(success=True)


@app.route("/photos/pending/<uuid:uuid>/")
def pending_photos_details(uuid):
    try:
        o = session.query(Photos).filter_by(status='pending', uuid=uuid).one()
        data = {
            "UUID": o.uuid,
            "url": o.url
        }
        return jsonify(data)
    except NoResultFound:
        abort(404)


@app.route("/photos/pending/")
def pending_photos():
    data = []
    query = session.query(Photos).filter_by(status='pending')
    data = []
    for o in query.all():
        data.append({
            "UUID": o.uuid,
            "url": o.url
        })
    return jsonify(data)


@app.route("/photos/completed/")
def completed_photos():
    data = []
    query = session.query(Photos).filter_by(status='completed')
    data = []
    for o in query.all():
        data.append({
            "UUID": o.uuid,
            "url": o.url
        })
    return jsonify(data)


@app.route("/photos/process/", methods=['POST',])
def process_queue():
    content = request.get_json()
    for photo_uuid in content.get('UUID', []):
        app.logger.info("Adding {} to image processor...".format(photo_uuid))
        channel.basic_publish(exchange='', routing_key=cfg.PHOTO_QUEUE_NAME, body=photo_uuid)
    return jsonify({"processing": 1})


if __name__ == '__main__':
    app.logger.setLevel(logging.INFO)

    signal.signal(signal.SIGINT, close_active_queue)
    signal.signal(signal.SIGTERM, close_active_queue)

    connection_parameters = pika.URLParameters(cfg.AMQP_URI)
    task_processor_connection = pika.BlockingConnection(connection_parameters)

    channel = task_processor_connection.channel()
    channel.queue_declare(queue=cfg.PHOTO_QUEUE_NAME)


    consumer_task = MyThread()
    consumer_task.start()

    engine = create_engine(cfg.PG_CONNECTION_URI, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()
    app.debug = True

    run_simple('0.0.0.0', 3000, app, use_reloader=False)
