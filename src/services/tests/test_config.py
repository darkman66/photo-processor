import unittest
from mock import patch
from services.config import get_default_env


class TestConfig(unittest.TestCase):

    def test_default_settings(self):
        env = get_default_env()

        self.assertEqual(env.STORAGE_DIR, "/waldo-app-thumbs")
        self.assertEqual(env.AMQP_USERNAME, "rabbitmq")
        self.assertEqual(env.AMQP_PASSWORD, "1234")
        self.assertEqual(env.AMQP_HOST, "localhost")

    def test_custom_values(self):
        with patch.dict('os.environ', {'STORAGE_DIR': 'newvalue'}):
            env = get_default_env()
            self.assertEqual(env.STORAGE_DIR, 'newvalue')
